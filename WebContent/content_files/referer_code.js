jQuery(function($) {
  var referer_cookie = document.cookie.match(/referer_code=([\d]+)/);
  var code = referer_cookie && referer_cookie[1];
  var referer_code_block = $('#referer_code_block'); // блок куда подставляется полученный код

  if (referer_code_block.length == 0) return; // ничего не делаем, если блока для подстановки кода нет на странице

  if (code) {
    referer_code_block.html(code); // подставляем код в шаблон
    var expired_date = new Date();
    expired_date.setTime(expired_date.getTime() + (1 * 0.5 * 60 * 60 * 1000)); // 30 минут
    document.cookie = "referer_code="+ code +"; expires="+ expired_date +"; path=/";
  } else {
    $.ajax({
      url:      "/referer_code",
      type:     'post',
      dataType: 'json',
      data:     {},
      success: function(response) {
        document.cookie = "referer_code="+ response.referer_code +"; expires="+ expired_date +"; path=/";
        referer_code_block.html(response.referer_code); // подставляем код в шаблон
      }
    });
  }
});
