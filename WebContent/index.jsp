<%@ page language="java" contentType="text/html; charset=utf-8"%>
<%@ page pageEncoding="UTF-8"%>
<%
	request.setCharacterEncoding("UTF-8");
%>
<%@ page
	import="kz.shop.engine.*,javax.servlet.http.Cookie"%>
<%
CheckUser user = new CheckUser();
int count=0,summ=0;
boolean done=false;
boolean nameExist=false;
String username="Войти";
if(user.check(request.getCookies())){
	done=true;
	count=user.user.getOrder_count();
	summ=user.user.getOrder_summ();
	if(!(user.user.getName_1()==null||"".equals(user.user.getName_1())||user.user.getName_1().isEmpty())){
		nameExist=true;
		username=user.user.getName_1();
		}
}

Cookie cookie = new Cookie("sid",user.sid);
response.addCookie(cookie);
%>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" class="js flexboxlegacy canvas canvastext no-touch inline-editor-disabled" data-leadgen="yes" data-advpp="yes"><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<style>
  .fc.old-theme-fix {
    overflow: visible;
  }
  .fc.old-theme-fix:after {
    clear: both;
    display: block;
    content: '';
  }
  .tt-dropdown-menu {
    max-height: 300px;
    overflow-y: auto;
    min-width: 160px;
    margin-top: 2px;
    padding: 5px 5px;
    background-color: #ffffff;
    border: 1px solid #cccccc;
    border: 1px solid rgba(0, 0, 0, 0.15);
    border-radius: 4px;
    -webkit-box-shadow: 0 6px 12px rgba(0, 0, 0, 0.175);
          box-shadow: 0 6px 12px rgba(0, 0, 0, 0.175);
    background-clip: padding-box;

  }
  .twitter-typeahead {
    width: 100%;
  }
  .twitter-typeahead .tt-query, .twitter-typeahead .tt-hint {
    margin-bottom: 0;
  }
  .tt-suggestion, .tt-footer {
    display: block;
    padding: 1px 0px 1px 10px;
  }
  .tt-suggestion.tt-is-under-cursor, .tt-footer.tt-is-under-cursor {
    color: #fff;
    background-color: #428bca;
  }
  .tt-suggestion.tt-is-under-cursor a, .tt-footer.tt-is-under-cursor a {
    color: #fff;
  }
  .tt-suggestion.tt-cursor, .tt-footer.tt-cursor {
    color: #fff;
    background-color: #428bca;
  }
  .tt-suggestion p, .tt-footer p {
    margin: 0;
  }
</style><meta name="js-evnvironment" content="production"><meta name="default-locale" content="ru"><meta name="insales-redefined-api-methods" content="[&quot;getPageScroll&quot;]"><script type="text/javascript" src="./Shop_files/be2e1c4ca5"></script><script src="./Shop_files/nr-963.min.js"></script><script type="text/javascript"  src="./Shop_files/insales_counter.js"></script><script src="./Shop_files/shop_bundle-5c6ed6ababcb9acd306b49538de15942.js" type="text/javascript"></script><style type="text/css">cufon{text-indent:0!important;}@media screen,projection{cufon{display:inline!important;display:inline-block!important;position:relative!important;vertical-align:middle!important;font-size:1px!important;line-height:1px!important;}cufon cufontext{display:-moz-inline-box!important;display:inline-block!important;width:0!important;height:0!important;overflow:hidden!important;text-indent:-10000in!important;}cufon canvas{position:relative!important;}}@media print{cufon{padding:0!important;}cufon canvas{display:none!important;}}</style>
        <!--InsalesCounter -->
        <script type="text/javascript">
        if (typeof(__id) == 'undefined') {
          var __id=84479;

          (function() {
            var ic = document.createElement('script'); ic.type = 'text/javascript'; ic.async = true;
            ic.src = '/javascripts/insales_counter.js?5';
            var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ic, s);
          })();
        }
        </script>
        <!-- /InsalesCounter -->

	
<script type="text/javascript">window.NREUM||(NREUM={});NREUM.info={"beacon":"bam.nr-data.net","errorBeacon":"bam.nr-data.net","licenseKey":"be2e1c4ca5","applicationID":"4617","transactionName":"dl5ZRkRbCFoBRxlCDFpBRB1EWwtCS1xYVQFNC1pbRUc=","queueTime":9,"applicationTime":353,"ttGuid":"","agentToken":null,"agent":""}</script>
<script type="text/javascript">window.NREUM||(NREUM={}),__nr_require=function(t,e,n){function r(n){if(!e[n]){var o=e[n]={exports:{}};t[n][0].call(o.exports,function(e){var o=t[n][1][e];return r(o||e)},o,o.exports)}return e[n].exports}if("function"==typeof __nr_require)return __nr_require;for(var o=0;o<n.length;o++)r(n[o]);return r}({1:[function(t,e,n){function r(){}function o(t,e,n){return function(){return i(t,[(new Date).getTime()].concat(u(arguments)),e?null:this,n),e?void 0:this}}var i=t("handle"),a=t(2),u=t(3),c=t("ee").get("tracer"),f=NREUM;"undefined"==typeof window.newrelic&&(newrelic=f);var s=["setPageViewName","setCustomAttribute","finished","addToTrace","inlineHit"],p="api-",l=p+"ixn-";a(s,function(t,e){f[e]=o(p+e,!0,"api")}),f.addPageAction=o(p+"addPageAction",!0),e.exports=newrelic,f.interaction=function(){return(new r).get()};var d=r.prototype={createTracer:function(t,e){var n={},r=this,o="function"==typeof e;return i(l+"tracer",[Date.now(),t,n],r),function(){if(c.emit((o?"":"no-")+"fn-start",[Date.now(),r,o],n),o)try{return e.apply(this,arguments)}finally{c.emit("fn-end",[Date.now()],n)}}}};a("setName,setAttribute,save,ignore,onEnd,getContext,end,get".split(","),function(t,e){d[e]=o(l+e)}),newrelic.noticeError=function(t){"string"==typeof t&&(t=new Error(t)),i("err",[t,(new Date).getTime()])}},{}],2:[function(t,e,n){function r(t,e){var n=[],r="",i=0;for(r in t)o.call(t,r)&&(n[i]=e(r,t[r]),i+=1);return n}var o=Object.prototype.hasOwnProperty;e.exports=r},{}],3:[function(t,e,n){function r(t,e,n){e||(e=0),"undefined"==typeof n&&(n=t?t.length:0);for(var r=-1,o=n-e||0,i=Array(o<0?0:o);++r<o;)i[r]=t[e+r];return i}e.exports=r},{}],ee:[function(t,e,n){function r(){}function o(t){function e(t){return t&&t instanceof r?t:t?u(t,a,i):i()}function n(n,r,o){t&&t(n,r,o);for(var i=e(o),a=l(n),u=a.length,c=0;c<u;c++)a[c].apply(i,r);var s=f[m[n]];return s&&s.push([w,n,r,i]),i}function p(t,e){g[t]=l(t).concat(e)}function l(t){return g[t]||[]}function d(t){return s[t]=s[t]||o(n)}function v(t,e){c(t,function(t,n){e=e||"feature",m[n]=e,e in f||(f[e]=[])})}var g={},m={},w={on:p,emit:n,get:d,listeners:l,context:e,buffer:v};return w}function i(){return new r}var a="nr@context",u=t("gos"),c=t(2),f={},s={},p=e.exports=o();p.backlog=f},{}],gos:[function(t,e,n){function r(t,e,n){if(o.call(t,e))return t[e];var r=n();if(Object.defineProperty&&Object.keys)try{return Object.defineProperty(t,e,{value:r,writable:!0,enumerable:!1}),r}catch(i){}return t[e]=r,r}var o=Object.prototype.hasOwnProperty;e.exports=r},{}],handle:[function(t,e,n){function r(t,e,n,r){o.buffer([t],r),o.emit(t,e,n)}var o=t("ee").get("handle");e.exports=r,r.ee=o},{}],id:[function(t,e,n){function r(t){var e=typeof t;return!t||"object"!==e&&"function"!==e?-1:t===window?0:a(t,i,function(){return o++})}var o=1,i="nr@id",a=t("gos");e.exports=r},{}],loader:[function(t,e,n){function r(){if(!h++){var t=y.info=NREUM.info,e=s.getElementsByTagName("script")[0];if(t&&t.licenseKey&&t.applicationID&&e){c(m,function(e,n){t[e]||(t[e]=n)});var n="https"===g.split(":")[0]||t.sslForHttp;y.proto=n?"https://":"http://",u("mark",["onload",a()],null,"api");var r=s.createElement("script");r.src=y.proto+t.agent,e.parentNode.insertBefore(r,e)}}}function o(){"complete"===s.readyState&&i()}function i(){u("mark",["domContent",a()],null,"api")}function a(){return(new Date).getTime()}var u=t("handle"),c=t(2),f=window,s=f.document,p="addEventListener",l="attachEvent",d=f.XMLHttpRequest,v=d&&d.prototype;NREUM.o={ST:setTimeout,CT:clearTimeout,XHR:d,REQ:f.Request,EV:f.Event,PR:f.Promise,MO:f.MutationObserver},t(1);var g=""+location,m={beacon:"bam.nr-data.net",errorBeacon:"bam.nr-data.net",agent:"js-agent.newrelic.com/nr-963.min.js"},w=d&&v&&v[p]&&!/CriOS/.test(navigator.userAgent),y=e.exports={offset:a(),origin:g,features:{},xhrWrappable:w};s[p]?(s[p]("DOMContentLoaded",i,!1),f[p]("load",r,!1)):(s[l]("onreadystatechange",o),f[l]("onload",r)),u("mark",["firstbyte",a()],null,"api");var h=0},{}]},{},["loader"]);</script>


	<!--[if IE]><script src="//html5shiv.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
  <title>Shop.kz</title>
  <meta name="keywords" content="Shop">
  <meta name="description" content="Shop">
  
    <link rel="icon" href="./Shop_files/default_favicon.ico" type="image/x-icon">
    <link rel="shortcut icon" href="./Shop_files/default_favicon.ico" type="image/x-icon">
    
  <link rel="stylesheet" href="./Shop_files/style.css" type="text/css" media="screen, projection">
  <link rel="stylesheet" href="./Shop_files/jquery.fancybox.css" type="text/css" media="screen, projection">
	<link href="./Shop_files/css" rel="stylesheet" type="text/css">

	
  <script src="./Shop_files/jquery.min.js" type="text/javascript"></script>
  <script type="text/javascript" src="./Shop_files/modernize.js"></script>
  <script type="text/javascript" src="./Shop_files/jquery.cookie.js"></script>
  <script type="text/javascript" src="./Shop_files/empty.js"></script>
  <script type="text/javascript" src="./Shop_files/cart.js"></script>
  <script type="text/javascript" src="./Shop_files/common.js"></script>
  <script type="text/javascript" src="./Shop_files/theme.js"></script>
  <script type="text/javascript" src="./Shop_files/jquery.cycle.all.js"></script>
   <script type="text/javascript" src="./Shop_files/jquery.fancybox.js"></script>
  <script type="text/javascript">
    var cv_currency_format = "{\"delimiter\":\"\",\"separator\":\".\",\"format\":\"%n&nbsp;%u\",\"unit\":\"\\u0440\\u0443\\u0431\",\"show_price_without_cents\":0}";
  </script>
  <style>
  #bdy{
  display:block;
  z-index:1;
  position:fixed;}
  #login{
   display:none;
  width:100%;
  height: 100%;
  position:fixed;
  top:0;
  left:0;
  z-index:1000;
  background: rgba(34,34,34,.6);.
  text-align: center;
  overflow: auto;
  white-space: nowrap;
  }


  #login_inner{
 
  	width: 300px;
    height: 400px;
    position: absolute;
    top: 50%;
    left: 50%;
    margin: -200px 0 0 -150px;
     text-align: left;
    vertical-align: middle;
    display: inline-block;
    border: 1px solid #d6d6d6;
    box-shadow: 0 1px 16px rgba(0,0,0,.25);
    background: #fff;
    white-space: normal;
        font-size: 13px;
    line-height: 18px;
    font-family: "Helvetica Neue",Arial,sans-serif;
    color: #222;
    transition: linear .3s margin-top;
  }
  #vhod{
  position:absolute;
  left:110px;
  top:-30px;
  }
  #vhod_1{
  position:absolute;
  left:65px;
  top:-30px;
  }
  .login_input{
  position:absolute;
	left: 10px;
	top: 50px;
  }
  .email{
  position:absolute;
	left: 30px;
	top: 70px;
  }
  .input{
  width:220px;
  height:20px;
  }
  #signup_enter{
  position:absolute;
  left: 65px; 
  heigh:10px;
  width:90px;
  }
  #auth_enter{
  position:absolute;
  left: 65px; 
  heigh:10px;
  width:90px;
  }
  #auth_answer{
  position:absolute;
  left:30px;
  color:red;
  }
  #signup_answer{
  position:absolute;
  left:30px;
  color:red;
  }
  #enter_form{
  z-index:2000;
  }
  #signup_form{
  z-index:2001;
  display:none;
  }
  </style>
  <script>

  $(document).ready (function(){ 
	  $("#change_1").click(function(){$("#enter_form").hide();$("#signup_form").show();});
	  $("#change_2").click(function(){$("#enter_form").show();;$("#signup_form").hide();});
	  $("#exit").click(function(){
		  document.cookie = "sid="
		  location.reload();
	  });
	  
	  
	  
	  $("#auth_enter").on("click",function(){
			 $.ajax ({
                 url: "Authorization",
                 data: ({
                	email: $("#auth_email").val(),
                	password: $("#auth_password").val()}),
                	
                 success: function (data){
                	 if(data=='not'){$("#auth_answer").text("e-mail или пароль не верны!");}
                	 else{$.cookie("sid", data); location.reload();}
                 }
             }); });
	  
	  
	  $("#signup_enter").on("click",function(){
		  if(!($("#signup_password1").val()==""||$("#signup_password2").val()=="")&&$("#signup_password1").val()==$("#signup_password2").val()){
				
			  $.ajax ({
              url: "Signup",
              data: ({
             	email: $("#signup_email").val(),
             	password: $("#signup_password1").val(),
             	name: $("#signup_name").val(),
             	sid: $.cookie('sid')
              }),
              success: function (data){
             	 if(data=='not1'){$("#signup_answer").text("e-mail введен не правильно");}
                 if(data=='not2'){$("#signup_answer").text("придумайте пароль");}
             	 if(data=='not3'){$("#signup_answer").text("напишите свое имя");}
             	 if(data=='not4'){$("#signup_answer").text("такой e-mail уже существует");}
             	 else{$("#signup_answer").text(data);}
             	 if(data=='yes'){location.reload();}
              }
          });}
          else{$("#signup_answer").text("Пароли не совпадают");}    
      });
	  
	  
  $("#enter").click(function(){$("#login").show();});
  });
  $(document).mouseup(function (e) {
	  var d=true;
	  var container = $("#login_inner");
	  if (container.has(e.target).length === 0){
	  	$("#login").hide();
	  }
	});
  </script>
</head>
  <body id="index">
    <noscript>
      &lt;div class="noscript-wrap"&gt;
        &lt;div class="center-content"&gt;
          &lt;div class="noscript"&gt;
            &lt;h2&gt;Внимание! В Вашем браузере отключена функция JavaScript!!!&lt;/h2&gt;
            &lt;br&gt;
            &lt;h3&gt;Пожалуйста включите JavaScript, затем обновите страницу.&lt;/h3&gt;
          &lt;/div&gt;
        &lt;/div&gt;
      &lt;/div&gt;
    </noscript>
<!--[if lt IE 7]> <div style=' clear: both; height: 59px; padding:0 0 0 15px; position: relative;'> <a href="http://windows.microsoft.com/en-US/internet-explorer/products/ie/home?ocid=ie6_countdown_bannercode"><img src="http://storage.ie6countdown.com/assets/100/images/banners/warning_bar_0016_russian.jpg" border="0" height="42" width="820" alt="You are using an outdated browser. For a faster, safer browsing experience, upgrade for free today." /></a></div> <![endif]-->
<div id="login">
<div id="login_inner">

<div class="login_input">


<div id="enter_form">
<h1 id="vhod">Вход</h1>
<div class="email">
<input id="auth_email" class="input" type='text' placeholder="E-mail"><br>
<br>
<input id="auth_password" class="input" type='password' placeholder="Пароль"><br><br>
<input id="auth_enter" type="button" value="Войти"><br><br>
<span id=auth_answer></span>
<br><br><br><br><br><br><br>
<a id="change_1" style="position:absolute; left: 72px; color:black;" href="#">Регистрация</a>
</div>
</div>



<div id="signup_form">
<h1 id="vhod_1">Регистрация</h1>
<div class="email">
<input id="signup_email" class="input" type='text' placeholder="E-mail"><br><br>
<input id="signup_password1" class="input" type='password' placeholder="Придумайте пароль"><br><br>
<input id="signup_password2" class="input" type='password' placeholder="Повторите пароль"><br><br>
<input id="signup_name" class="input" type='text' placeholder="Ваше имя"><br><br>
<input id="signup_enter" type="button" value="Регистрация"><br><br>
<span id="signup_answer"></span>
<br><br>
<a id="change_2" style="position:absolute; left: 90px; color:black;" href="#">Вход</a>
</div>
</div>




</div>
</div>
</div>


<div class="bdy">

<div id="wrapper">
  <header id="header">
    <div class="top-header">
      <div class="center-content">
        <div class="header-top">
          <div class="fl logo-wrap">
          <a href="http://localhost:8080/Shop" class="logo text">
            <span>Shop.kz</span>
          </a>
          </div>
          <div class="header-info fl">
            <ul>
					
 <li><a href="http://localhost:8080/Shop" title="Главная" class="active">Главная</a></li>
 
 <li><a href="#" title="Условия доставки" class="">Условия доставки</a></li>
                
 <li><a href="#" title="Обратная связь" class="">Обратная связь</a></li>

             </ul>
            
            <div class="phone" >
            <span class="hidden">(000) 000-00-00</span>
            <span class="hidden">Shop</span>
            
          </div>
            
          </div>
          <div class="cart fr">
            
      						<a href="#" class="basket"> <span id="cart_items_count"><%=count%></span>
						</a> <a href="#" class="price" id="cart_total_price"><%=summ%>&nbsp;тг</a>
					</div>
					<div class="search-block fr">
						<ul class="fr login">
							
							<%if(!done)	{%><li><a id="enter" href="#">Войти</a></li><%}
								else 	{if(nameExist){%><li><%=username%></li>
										   <li><a id="exit" href="#">Выйти</a><%}
										   else{%><li><a id="enter" href="#">Войти</a></li><%}
								} %>
							<li><a href="">Корзина</a></li>
              
            </ul>
            <div class="search clear">
              <form action="#" method="get">
                
                <input type="text" class="txt" name="q" value="Поиск" onfocus="if(this.value==this.defaultValue){this.value=&#39;&#39;;}" onblur="if(this.value==&#39;&#39;){this.value=this.defaultValue;}">
                <input type="submit" class="but" value="">
              </form>
            </div>
          </div>
          <div class="clear"></div>
        </div>
      </div>
    </div>
    <div class="bottom-header clear">
			<div class="center-content">
			 <nav>
			 <ul class="head-catalog">
               
               <li><a>Мужчинам</a>
                 
                 <ul class="subnav">
                   
                   <li>
                     <div>Одежда</div>
                     
                     <ul>
                       
                       <li><a href="clothes?title=m_polo" class="">Футболки и поло</a></li>
                       
                       <li><a href="clothes?title=m_pants" class="">Брюки</a></li>
                         
                       <li><a href="clothes?title=m_shirts" class="">Рубашки</a></li>
                         
                       
                       
                     </ul>
                     
                   </li>
                   
                   <li>
                     <div>Обувь</div>
                     
                     <ul>
                       
                       <li><a href="clothes?title=m_shoes" class="">Туфли</a></li>
                       
                       <li><a href="clothes?title=m_gumshoes" class="">Кроссовки и кеды</a></li>
                       
                       <li><a href="clothes?title=m_zippers" class="">Ботинки</a></li>
                       
                     </ul>
                     
                   </li>
                      <li>
                     <div>Аксессуары</div>
                     
                     <ul>
                       
                       <li><a href="clothes?title=m_bags" class="">Сумки</a></li>
                       
                       <li><a href="clothes?title=m_glasses" class="">Очки</a></li>
                       
                       <li><a href="clothes?title=m_watches" class="">Часы</a></li>
                       
                     </ul>
                     
                   </li>
                 </ul>
                 
               </li>
               
               <li><a>Женщинам</a>
                 
                 <ul class="subnav">
                   
                   <li>
                     <div>Одежда</div>
                     
                     <ul>
                       
                       <li><a href="clothes?title=w_dresses" class="">Платья</a></li>
                       
                       <li><a href="clothes?title=w_outerwear" class="">Верхняя одежда</a></li>
                         
                       <li><a href="clothes?title=w_blouses" class="">Блузы и рубашки</a></li>                        
                       
                       
                     </ul>
                     
                   </li>
                   
                   <li>
                     <div>Обувь</div>
                     
                     <ul>
                       
                       <li><a href="clothes?title=w_slippers" class="">Туфли</a></li>
                       
                       <li><a href="clothes?title=w_toeshoes" class="">Балетки</a></li>
                       
                       <li><a href="clothes?title=w_sandals" class="">Босоножки</a></li>
                       
                     </ul>
                     
                   </li>
                      <li>
                     <div>Аксессуары</div>
                     
                     <ul>
                       
                       <li><a href="clothes?title=w_bags" class="">Сумки</a></li>
                       
                       <li><a href="clothes?title=w_glasses" class="">Очки</a></li>
                       
                       <li><a href="clothes?title=w_wathes" class="">Часы</a></li>
                       
                     </ul>
                     
                   </li>
                 
                 </ul>
                 
               </li>
               
               <li><a>Детям</a>
                 
                 <ul class="subnav">
                   
                    <li>
                     <div>Мальчикам</div>
                     
                     <ul>
                       
                       <li><a href="clothes?title=b_shirts" class="">Футболки</a></li>
                       
                       <li><a href="clothes?title=b_outerwear" class="">Верхняя одежда</a></li>
                         
                       <li><a href="clothes?title=b_shoes" class="">Ботинки</a></li>
                         
                       
                       
                     </ul>
                     
                   </li>
                   
                   <li>
                     <div>Девочкам</div>
                     
                     <ul>
                       
                       <li><a href="clothes?title=g_dresses" class="">Платья и сарафаны</a></li>
                       
                       <li><a href="clothes?title=g_skirts" class="">Юбки</a></li>
                       
                       <li><a href="clothes?title=g_slippers" class="">Туфли</a></li>
                       
                     </ul>
                     
                   </li>
                     
                   
                 </ul>
                 
               </li>
               
               
               
			 </ul>
			 <div class="clear"></div>
			 <a class="cat-arrow" href="#" style="display: none;"></a>
			 </nav>
			</div>
      <div class="dashed"></div>
    </div>
  </header><!-- #header-->
  
  
  <div class="slider-wrap">
    <div class="slider">
      <ul id="slider" style="position: relative; width: 980px; height: 264px;">
        
        <li style="position: absolute; top: 0px; left: 0px; display: block; z-index: 500; opacity: 0.986388; width: 980px; height: 264px;">
          
          <a href="#"><img src="./Shop_files/loaded_main_image_1.jpg"></a>
          
        </li>
        

      
      <li style="position: absolute; top: 0px; left: 0px; display: block; z-index: 500; opacity: 0.0136121; width: 980px; height: 264px;">
        
        <a href="#"><img src="./Shop_files/loaded_main_image_2.jpg"></a>
        
      </li>
      

      

      

      
    </ul>
    <div class="slider-nav">
    <a id="slider-next" href="#"></a>
    <a id="slider-prev" href="#"></a>
    </div>
  </div>
  </div>
  <div class="dashed"></div>
  
  
  <section id="middle">
    <div id="container">
      <div id="content"><h2>Новости<br></h2>Новостей пока нет</div><!-- #content-->
    </div>
    
	</section>
  
 
  
	<footer id="footer">
		<nav>
		 <ul class="fl">

           <li><a href="#" title="Контакты" class="">Контакты</a></li>

           <li><a href="http://localhost:8080/Shop" title="Главная страница" class="active">Главная страница</a></li>

		 </ul>
      </nav><br>
       <div class="footer1">
        </div>
	</footer>
</div>
</div>
</body>
</html>