function delete_by_url(obj) {
    var f = document.createElement('form'); 
    f.style.display = 'none'; 
    obj.parentNode.appendChild(f); 
    f.method = 'POST'; 
    // В стандартных шаблонах ссылка после инициализации хранится в rel,
    // но для обратной совместимости оставляем href
    f.action = $(obj).attr('rel') || $(obj).attr('href');
    var m = document.createElement('input'); 
    m.setAttribute('type', 'hidden'); 
    m.setAttribute('name', '_method'); 
    m.setAttribute('value', 'delete'); 
    f.appendChild(m);
    f.submit();
}

jQuery(function($) {

    $("#cart_update").click(function(event) {
        $('#cartform').submit();
        event.preventDefault();
    });
	
    // Удалить
    $("a[id^=delete_]").each(function() {
        // Устанавливаем ссылку на #, 
        // чтобы застраховаться от открывания в новом окне.
        if( $(this).attr('href') != '#' ) {
          $(this).attr('rel', $(this).attr('href'));
        }
        $(this).attr('href','#');
        $(this).click(function(event) {
            delete_by_url(this);
            event.preventDefault();
        });
    });

    // Пересчитать
    $("input[name^=cart]").bind("change keyup", function() {
        if ($(this).attr("name").match("parameters")) return;
        $(".cart_update").show();
    });

    // или при выборе способа доставки
    $("input[name*=delivery_variant_id]").click(function() {
        $(".cart_update").show();
    });
})
;
