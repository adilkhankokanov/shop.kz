package kz.shop.servlets;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


@WebServlet("/CheckClotheExistence")
public class CheckClotheExistence extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
  
    public CheckClotheExistence() {
        super();
    }

	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		String id = request.getParameter("id").trim();
		
	    
		if("16".equals(id)){
			response.setContentType("text/plain");
			response.getWriter().write("1");
		}
		else{
			response.setContentType("text/plain");
			response.getWriter().write("2");
		}
	}

	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

}
