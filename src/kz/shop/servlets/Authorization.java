package kz.shop.servlets;

import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DateFormat;
import java.util.Date;
import java.util.Random;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import com.mysql.jdbc.Statement;
import kz.shop.engine.Connector;
import kz.shop.utility.MD5;

@WebServlet("/Authorization")
public class Authorization extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    public Authorization() {
        super();
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String email = request.getParameter("email").trim();
		String password = request.getParameter("password").trim();
		boolean exit=false;
		if(email.isEmpty()||password.isEmpty()){
			response.setContentType("text/plain");
			response.getWriter().write("not");
			exit=true;
		}
		
		if(!exit){	
			String sid="";
			boolean exist=false;
			try {
				Connector con = new Connector();
				Statement myStmt1 = (Statement) con.connect();
				ResultSet myRs = myStmt1.executeQuery("Select * from shop.users where email='"+email+"' and password=md5('"+password+"')");
				while (myRs.next()) 
					{
					
						sid=myRs.getString("sid");
						if((sid.isEmpty())||(sid=="")||sid==null){
							String randString = DateFormat.getDateInstance().format(new Date()) + new Random(System.currentTimeMillis());
							sid=(new MD5().getHash(randString));
							myStmt1.executeUpdate("UPDATE shop.users SET sid='"+sid+"' WHERE email='"+email+"'");
						}
						exist=true;
					}
				
			} catch (SQLException e) {
				e.printStackTrace();
			}
			
			
			
			if(exist){
				response.setContentType("text/plain");
				response.getWriter().write(sid);
			}
			else{
				response.setContentType("text/plain");
				response.getWriter().write("not");
			}
		}

	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}



}
