package kz.shop.engine;

import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import com.mysql.jdbc.Connection;

public class Connector {
	  public Statement connect() throws SQLException {
	  Connection con = null;
	  Statement myStmt = null;
	      try {
	      Class.forName("com.mysql.jdbc.Driver"); 
	      con = (Connection) DriverManager.getConnection("jdbc:mysql://localhost:3306/shop", "root", "root");
	      myStmt = con.createStatement();
	     
	    } catch (Exception exc) {
	      System.out.println(exc);
	    }
	      return myStmt;
	  }
	}