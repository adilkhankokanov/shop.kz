package kz.shop.engine;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DateFormat;
import java.util.Date;
import java.util.Random;

import javax.servlet.http.Cookie;
import com.mysql.jdbc.Statement;
import kz.shop.entity.User;
import kz.shop.utility.MD5;

public class CheckUser {
	public User user;
	public String sid;
	
	public boolean check(Cookie[] cookies) throws SQLException {
		boolean userExist=false;
		boolean sidExist=false;
		Connector users = new Connector();
		Statement myStmt1 = (Statement) users.connect();
		String cookieSID="";
		users = new Connector();
				
		for (int i = 0; i < cookies.length; i++) 
			if (cookies[i].getName().equals("sid")) {
				cookieSID=cookies[i].getValue();
				sidExist=true;
				break;
			}
		if(sidExist) {
			try{
				ResultSet myRs = myStmt1.executeQuery("Select * from shop.users");
				
				while (myRs.next()) {
					if (myRs.getString("sid").equals(cookieSID)) {
						user = new User(myRs.getInt("id"), myRs.getString("email"), myRs.getString("password"),
								myRs.getString("name_1"), myRs.getString("name_2"), myRs.getString("name_3"),
								myRs.getString("tel"), myRs.getString("sid"), myRs.getDate("birthday"),
								myRs.getString("gender"), myRs.getInt("order_count"), myRs.getInt("order_summ"));
						userExist=true;
						break;
					}

				}
				if (userExist) {
					sid = user.getSid();
				}
			}catch(Exception e){
				System.out.println(e + "____+++++");
			}
			return true;
			}
		else{
			try{
			String randString = DateFormat.getDateInstance().format(new Date()) + new Random(System.currentTimeMillis());
			sid = (new MD5().getHash(randString));
			myStmt1.executeUpdate("insert into shop.users (sid) values ('" + sid + "')");
			ResultSet myRs2 = myStmt1.executeQuery("select * from shop.users");
			while (myRs2.next())
				if (myRs2.getString("sid").equals(sid)) {
					user = new User(myRs2.getString("sid"));
				}
			
			}catch(Exception e){
				System.out.println(e + "____*******");
			}
			return false;
		}
	}
}
