package kz.shop.entity;
import java.sql.Date;

public class User {
	private int id;
	private String email;
	private String password;
	
	private String name_1;
	private String name_2;
	private String name_3;
	private String tel;
	private String sid;
	private Date birthday;
	private String gender;
	private int order_count;
	private int order_summ;
	
	public User(String sid){
		super();
		this.sid=sid;
		this.id=0;
		this.email="";
		this.password="";
		this.name_1="Войти";
		this.name_2="";
		this.name_3="";
		this.tel="";
		this.birthday=new Date(0);
		this.gender="";
		this.order_count=0;
		this.order_summ=0;
	}
	public User(){
		super();
	}
	public User(int id,String email,String password,String name_1,String name_2,String name_3,String tel,String sid,
			Date birthday,String gender,int order_count,int order_summ){
		super();
		this.id=id;
		this.email=email;
		this.password=password;
		this.name_1=name_1;
		this.name_2=name_2;
		this.name_3=name_3;
		this.tel=tel;
		this.sid=sid;
		this.birthday=birthday;
		this.gender=gender;
		this.order_count=order_count;
		this.order_summ=order_summ;
	}
//getters
	public int getId() {
		return id;
	}
	public String getEmail() {
		return email;
	}
	public String getPassword() {
		return password;
	}
	public String getName_1() {
		return name_1;
	}
	public String getName_2() {
		return name_2;
	}
	public String getName_3() {
		return name_3;
	}
	public String getTel() {
		return tel;
	}
	public String getSid() {
		return sid;
	}
	public Date getBirthday() {
		return birthday;
	}
	public String getGender() {
		return gender;
	}
	public int getOrder_count() {
		return order_count;
	}
	public int getOrder_summ() {
		return order_summ;
	}
}