package kz.shop.entity;


public class Clothe {
	
	private int id;
	private String cathegory;
	private int cost;
	private String pic_url;
	private String description;
	private String producer;
	private String color;
	private String gender;
	private int quantity;
	

	public Clothe(){
		super();
		this.id=0;
		this.cathegory="unknown";
		this.cost=0;
		this.pic_url="unknown";
		this.description="unknown";
		this.producer="unknown";
		this.color="unknown";
		this.gender="unknown";
		this.quantity=0;
	}

	public Clothe(int id,String cathegory,int cost,String pic_url,String description,
			String producer,String color,String gender,int quantity){
		super();
		this.id=id;
		this.cathegory=cathegory;
		this.cost=cost;
		this.pic_url=pic_url;
		this.description=description;
		this.producer=producer;
		this.color=color;
		this.gender=gender;
		this.quantity=quantity;
	}

//getters and setters
	public int getQuantity() {
		return quantity;
	}
	public String getCathegory() {
		return cathegory;
	}
	public int getCost() {
		return cost;
	}
	public String getPic_url() {
		return pic_url;
	}
	public String getDescription() {
		return description;
	}
	public String getProducer() {
		return producer;
	}
	public String getColor() {
		return color;
	}
	public int getId() {
		return id;
	}
	public String getGender() {
		return gender;
	}
}
