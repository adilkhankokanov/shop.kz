package kz.shop.utility;

public class ClotheType {
	public String gender(String g){
		switch(g){
		case("m"): return "Мужчинам";
		case("w"): return "Женщинам";
		case("b"): return "Мальчикам";
		case("g"): return "Девочкам";
		default: return "undefined";}
	}
	public String clotheType(String title){
			String type="";
			switch (title) {
			case "m_polo" : type="Футболки и поло"; break;
			case "m_pants" : type="Брюки"; break;
			case "m_shirts" : type="Рубашки"; break;
			case "m_shoes" : type="Туфли"; break;
			case "m_gumshoes" : type="Кроссовки и кеды"; break;
			case "m_zippers" : type="Ботинки"; break;
			case "m_bags" : type="Сумки"; break;
			case "m_glasses" : type="Очки"; break;
			case "m_watches" : type="Часы"; break;
			case "w_dresses" : type="Платья"; break;
			case "w_outerwear" : type="Верхняя одежда"; break;
			case "w_blouses" : type="Блузы и рубашки"; break;
			case "w_slippers" : type="Туфли"; break;
			case "w_toeshoes" : type="Балетки";	break;
			case "w_sandals" : type="Босоножки"; break;
			case "w_bags" : type="Сумки"; break;
			case "w_glasses" : type="Очки"; break;
			case "w_wathes" : type="Часы"; break;
			case "b_shirts" : type="Футболки"; break;
			case "b_outerwear" : type="Верхняя одежда"; break;
			case "b_shoes" : type="Ботинки"; break;
			case "g_dresses" : type="Платья и сарафаны"; break;
			case "g_skirts" : type="Юбки"; break;
			case "g_slippers" : type="Туфли"; break;
			default: type="undefined";
		};
			return type;
		}
}