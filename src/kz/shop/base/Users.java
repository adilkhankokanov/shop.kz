package kz.shop.base;

import java.util.List;
import kz.shop.entity.User;

public class Users {
	public static List <User> users;
	
	public static void addUser(User user){
		users.add(user);
	}

	public static User getUser(int i) {
		return users.get(i);
	}
}
