package kz.shop.base;

import kz.shop.entity.Clothe;

public class Clothes {
	private static int count=0;
	private static Clothe[] clothes;
	
	public static int getSize(){
		return clothes.length;
	}
	public static void addClothe(Clothe clothe){
		clothes[count]=clothe;
		count++;
	}

	public static Clothe getClothe(int i) {
		return clothes[i];
	}
	
}